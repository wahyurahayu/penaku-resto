<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Penaku</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <?php require_once ('layout/navbar.php')?>

        <!-- menu -->
        <div class="container">
      <div class="row row-cols-1 row-cols-md-3 g-4">
             <div class="col">
          <div class="card">
            <img src="assets/images/boba macta.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Boba Grentee</h5>
              <p class="card-text">RP.15.000</p>
            <a href="#" class="btn btn-success btn-sm">Tambahkan</a>
              <a href="#" class="btn btn-primary btn-sm">Batal</a>

            </div>
          </div>
        </div>
        <div class="col">
          <div class="card">
            <img src="assets/images/boba sugar.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">boba sugar</h5>
              <p class="card-text">RP.10.000</p>
            <a href="#" class="btn btn-success btn-sm">Tambahkan</a>
              <a href="#" class="btn btn-primary btn-sm">Batal</a>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card">
            <img src="assets/images/sirupstwb.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">sirup strowberi</h5>
              <p class="card-text">RP.15.000</p>
            <a href="#" class="btn btn-success btn-sm">Tambahkan</a>
              <a href="#" class="btn btn-primary btn-sm">Batal</a>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card">
            <img src="assets/images/capucino.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Capucino</h5>
              <p class="card-text">RP.8.000</p>
            <a href="#" class="btn btn-success btn-sm">Tambahkan</a>
              <a href="#" class="btn btn-primary btn-sm">Batal</a>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card">
            <img src="assets/images/es buah.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Es Buah</h5>
              <p class="card-text">RP.15.000</p>
            <a href="#" class="btn btn-success btn-sm">Tambahkan</a>
              <a href="#" class="btn btn-primary btn-sm">Batal</a>

            </div>
          </div>
        </div>
        <div class="col">
          <div class="card">
            <img src="assets/images/es jeruk.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Es Jeruk</h5>
              <p class="card-text">RP.10.000</p>
              <a href="#" class="btn btn-success btn-sm">Tambahkan</a>
              <a href="#" class="btn btn-primary btn-sm">Batal</a>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card">
            <img src="assets/images/es cream.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Es Cream</h5>
              <p class="card-text"><RP class="15 000"></RP></p>
              <a href="#" class="btn btn-success btn-sm">Tambahkan</a>
              <a href="#" class="btn btn-primary btn-sm">Batal</a>
            </div>
          </div>
        </div>
      </div>  
    </div>

              <a href="makanan.php" class="btn btn-primary ">Back</a>
              <a href="snack.php" class="btn btn-primary ">Next</a>

  
    <!-- end menu -->



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>