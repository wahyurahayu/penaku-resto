<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Penaku</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <?php require_once ('layout/navbar.php')?>
    <!-- <div class="container">
    <div class="wrap">
                <div class="main">
                    <div class="name">New Post</div>
                    <a href="add-pesanan.php" class="tombol">Back</a>
                </div>


                <?php
                    $notif = '';
                    if(isset($_POST['tombol'])){
                        $judul          = $_POST['judul'];
                        $deskripsi      = $_POST['deskripsi'];
                        $menu           = $_POST['menu'];
                        $artikel        = $_POST['artikel'];
                        $reporter       = $_POST['reporter'];

                        $query = "INSERT INTO news(judul,deskripsi,artikel,author,kategori) 
                                  VALUES ('$judul','$deskripsi','$artikel','$reporter','$menu')";

                        mysqli_query($connect, $query);
                        $notif = 'Artikel berhasil disimpan';
                    }
                ?>


                <form action="" method="post">
                <div style="color:green"><?=$notif?></div>
                <div class="post">
                    <div class="form">
                        <div class="label">Judul</div>
                        <div class="input">
                            <input type="text" name="judul" placeholder="Judul">
                        </div>
                    </div>
                </div>

                <div class="des">
                    <div class="form">
                        <div class="label">Deskripsi</div>
                        <div class="input1">
                            <input type="text" name="deskripsi" placeholder="Deskripsi">
                        </div>
                    </div>
                    <div class="form">
                        <div class="label">Menu</div>
                        <select id="menu" name="menu">
                            <option value="" hidden>Select</option>
                            <option value="">makanan</option>
                            <option value="">minuman</option>
                            <option value="">snack</option>
                        </select>
                    </div>
                </div>

                <div class="area">
                    <textarea name="artikel" id="editor" cols="130" rows="100"></textarea>
                    <div class="table">
                        <a href="" class="tombol">Add Table of Content</a>
                    </div>
                </div>

                <div class="bawah">
                    <div class="form">
                        <div class="label">Image</div>
                        <div class="file">
                            <input type="file">
                        </div>
                    </div>
                </div>

                <div class="bawah1">
                    <div class="form">
                        <div class="label">Image Title</div>
                        <div class="input2">
                            <input type="text" placeholder="Image Title">
                        </div>
                    </div>
                    <div class="form">
                        <div class="label">Url</div>
                        <div class="input2">
                            <input type="url" class="url">
                        </div>
                    </div>
                    <div class="form">
                        <div class="label">Reporter</div>
                        <div class="input2">
                            <input type="text" name="reporter" placeholder="Reporter">
                        </div>
                    </div>
                </div>

                <div class="new">
                    <button type="submit" name="tombol" class="tombol">Create</button>
                </div>

                </form>

            </div>
            

        </div>
    </div>

    </div>
    

     -->
    <!-- <div class="container"> -->
    <div class="alert alert-dark" role="alert">
        <!-- <h4 class="alert-heading">Well done!</h4>
        <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
        <hr> -->
        <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
    </div>
    <!-- </div> -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>