<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <title>Document</title>
  <?php require_once ('layout/database.php')?>
<?php
    $notif = '';
    if(isset($_POST['tombol'])){
        $nomeja        = $_POST['no meja'];
        $namapesanan   = $_POST['nama pesanan'];
        $pesanan       = $_POST['pesanan'];
        $jumlah        = $_POST['jumlah'];
        $tanggal       = $_POST['tanggal'];

        $query = "INSERT INTO pesanan ( no meja,nama pesanan,jumlah,tanggal,pesanan) 
                  VALUES ('$nomeja','$namapesanan','$jumlah','$tanggal','$pesanan')";

        mysqli_query($connect, $query);
        $notif = 'menu berhasil disimpan';
    }
?>
</head>
<body>
  <div class="container">
    <div class="col mt-5">
      <form class="row g-3 ">
        <div style="color:green"><?=$notif?></div>
        <div class="col-md-2">
          <label for="inputEmail4" class="form-label">Id</label>
          <input type="text" class="form-control" id="inputEmail4">
        </div>
        <div class="col-md-2">
          <label for="inputPassword4" class="form-label">No meja</label>
          <input type="number " name="nomeja" placeholder="no meja" class="form-control" id="inputPassword4">
        </div>
        <div class="col-md-6">
          <label for="inputAddress" class="form-label">Nama Pemesan</label>
          <input type="text" name="namapemesan" placeholder="namapemesan"class="form-control" id="inputAddress" placeholder="nama anda">
        </div>
        <div class="col-md-2">
          <label for="inputAddress2" class="form-label">Tanggal</label>
          <input type="text" name="tanggal" placeholder="tanggal" class="form-control" id="inputAddress2" placeholder=" ">
        </div>
        <div class="col-md-4">
          <label for="inputState" class="form-label">Pesanan</label>
          <input type="text" name="pesanan" placeholder="pesanan"class="form-control" id="inputCity">   
          <!-- <select id="inputState" class="form-select">
            <option selected>Menu</option>
            <option value="makanan.php"  >makanan</option>
            <option value="minuman.php" >minuman</option>
            <option value="snack.php"  >snack</option>
          </select> -->
        </div>
        <div class="col-md-6">
          <label for="inputCity" class="form-label">Jumlah</label>
          <input type="text" name="jumlah" placeholder="jumlah"class="form-control" id="inputCity">
        </div>
        <div class="col-md-2">
          <label for="inputZip" class="form-label">Aksi</label>
          <input type="text" class="form-control" id="inputZip">
        </div>
        <div class="col-12">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
              Check me out
            </label>
          </div>
        </div>
        <div class="col-12">
        <a href="" name="tombol" class="btn btn-primary ">Sign in</a>
          <!-- <button type="submit" class="btn btn-primary">Sign in</button> -->
          <a href="index.php"  class="btn btn-primary ">Back</a>
        </div>
      </form>
    </div>
</div>



<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

</body>
</html>
