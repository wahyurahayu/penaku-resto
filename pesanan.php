<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Penaku</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <?php require_once ('layout/navbar.php')?>
    <?php require_once ('layout/database.php')?>
    
    <!-- Pesanan -->
        <!-- menu -->
        <div class="container">
        <table class="table table-border:100px" >
            <thead class="thead-dark">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">No Meja</th>
                <th scope="col">Nama Pemesan</th>
                <th scope="col">Pesanan</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Status</th>
                <th scope="col">aksi</th>

              </tr>
            </thead>
              <!-- delete -->
    <?php
                    if(isset($_GET['delete'])){
                      $delete = $_GET['delete'];
                      $sql = "DELETE FROM tab_pesanan WHERE id = '$delete' ";
                      mysqli_query($connect, $sql);
                      
                      header('location:pesanan.php?message=Data Berhasil diHapus');

                  }

                  $user = mysqli_query($connect, "SELECT * FROM tab_pesanan");
                  if(mysqli_num_rows($user) > 0){
                      $no = 1;
                      while($data = mysqli_fetch_assoc($user)){
                          ?>
                            <tbody>
                              <tr>
                                <th scope="row"><?=$no++?></th>
                                <td><?=$data['no meja']?></td>
                                <td><?=$data['nama pemesan']?></td>
                                <td><?=$data['pesanan']?></td>
                                <td><?=$data['jumlah']?></td>
                                <td><?=$data['tanggal']?></td>
                                <td><?=$data['status']?></td>
                                <td>
                                <a href="?delete=<?= $data['id']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin akan menghapus data user ini?')">Hapus Data</a>
                                </td>
                              </tr>
                            </tbody>
                          <?php
                      }
                  }
                    ?>
    <!-- end delete -->
          </table>
    </div>
    <!-- end menu -->
    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>